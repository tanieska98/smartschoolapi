package com.developerfriends.smartschool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartSchoolApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartSchoolApiApplication.class, args);
	}
}
